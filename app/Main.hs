
{-# LANGUAGE OverloadedStrings #-}


module Main where

import Options.Applicative

import Cron
import Deploy
import Unisys

import Control.Monad.Trans.Reader (runReaderT)
import Control.Monad (void, when)
import Data.List
import Data.Text.IO as T

import System.FilePath

newtype Options = Opt { optCommand :: Command }

data Command = Deploy
             | Cron
             | Unisys { sync :: Bool }


unisysCmd :: Parser Command
unisysCmd = Unisys
         <$> switch
         (long "sync"
         <> short 's'
         <> help "Sync all prfs")

commandParser :: Parser Options
commandParser = Opt
  <$> hsubparser
  ( command "deploy" (info (pure Deploy) (progDesc "deploy all symlinks"))
    <>
    command "cron" (info (pure Cron) (progDesc "build and install user crontabs"))
    <>
    command "unisys" (info unisysCmd (progDesc "unison administration")))

dhallConfigDir :: FilePath
dhallConfigDir = "/home/george/assets/config/"

deployConfDhall :: FilePath
deployConfDhall = dhallConfigDir </> "deploy.dhall"

cronConfDhall :: FilePath
cronConfDhall = dhallConfigDir </> "cron.dhall"

unisysDhall :: FilePath
unisysDhall = dhallConfigDir </> "unisys.dhall"

-- unisysApp :: Bool -> IO ()
-- unisysApp sync = do
--   _ <- buildPrfs unisysDhall
--   when sync syncPrfs

main :: IO ()
main = start =<< execParser opts
  where
    opts = info (commandParser <**> helper)
      ( fullDesc
        <> progDesc "Administrative tasks"
        <> header "admin - carry out administative tasks"
      )

start :: Options -> IO ()
start cli =
  case optCommand cli of
    Deploy -> deployEnv deployConfDhall >>= runReaderT deploy
    Cron   -> cronEnv cronConfDhall  >>= runReaderT setCron
    Unisys sync  -> unisysEnv unisysDhall >>= runReaderT (unisys sync)



