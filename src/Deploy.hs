{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE NamedFieldPuns #-}



module Deploy
    ( deploy
    , deployEnv
    ) where

import Target

import Control.Exception

import Control.Monad.Trans.Reader
import Control.Monad.Trans.Class (lift)

import Data.Function ((&))
import Data.Foldable

import Data.Text as T
import Data.Text.IO as TIO

import Data.Bool as DB
import Data.List as DL (map, filter, intercalate, zip)


import Formatting

import qualified Dhall as Dh

import System.Directory ( removeFile, copyFile, doesFileExist )
import System.FilePath
import System.Posix.Files (FileStatus,
                           createSymbolicLink,
                           isSymbolicLink,
                           getSymbolicLinkStatus,
                           isRegularFile,
                           isDirectory)

import System.Posix.Directory (createDirectory)
import System.Posix.Types (CMode(..))

import System.IO.Error

import qualified System.Process as Proc
import GHC.RTS.Flags (MiscFlags(linkerAlwaysPic))


       
data Action = Symlink
            | SymlinkToDotted
            | Copy
            | CopyToDotted
            deriving (Show, Dh.Generic)


data DeploySpec = Dir { originDir :: Text
                      , destinationDir :: Text
                      , files :: [ Text ]
                      , targets :: [ Target ]
                      , action:: Action
                      }
                | File { origin :: Text
                       , destination :: Text
                       , targets :: [ Target ]
                       , action :: Action
                       }
                | EnsureDirs { dirnames :: [ Text ]
                             , targets :: [ Target ]
                             }
                deriving (Show,Dh.Generic)

-- data DeployItem = DI { category::Text
--                      , targets :: [ Target ]
--                      , link :: DeploySpec
--                      }
--                   deriving (Show, Dh.Generic)

instance Dh.FromDhall Action
instance Dh.FromDhall DeploySpec

data FS = DNE
        | Regular
        | SymLink
        | Directory
  deriving (Show, Eq)

instance Targetable DeploySpec where
  getTargets = targets

deployEnv :: FilePath -> IO [DeploySpec]
deployEnv = Dh.inputFile Dh.auto


deploy :: ReaderT [ DeploySpec ] IO ()
deploy = do
  env <- ask
  pred <- lift targetsMatch
  lift $ traverse_ runDeploy $ DL.filter pred env


classifyFilePath :: FilePath -> IO FS
classifyFilePath fp =
  do
    let test s
           | isRegularFile s = Regular
           | isDirectory s = Directory
           | isSymbolicLink s = SymLink
           | otherwise = DNE
    res <- try (getSymbolicLinkStatus fp) :: IO (Either SomeException FileStatus)
    case res of
      Left e -> return DNE
      Right tt -> return $ test tt

runDeploy :: DeploySpec -> IO ()
runDeploy ls =

  case ls of
    Dir {originDir, destinationDir, files , action } ->
      traverse_ (\f -> deployAction originDir f destinationDir (maybeDot f))  files
      where pairs = DL.zip
                    ((originDir `join`) <$> files)
                    ((destinationDir `join`) <$> files)
  
            maybeDot :: Text -> Text
            maybeDot f = case action of
              SymlinkToDotted -> "." <> f
              CopyToDotted -> "." <> f
              _ -> f

    File { origin, destination } ->
      deployAction "" origin "" destination


    EnsureDirs { dirnames } ->
      traverse_ ensureDir dirnames
  
  where copier :: Text -> Text -> Text -> Text -> IO ()
        copier d1  f1 d2 f2 = do
          announce d1 f1 d2 f2
          _ <- try $ removeFile (T.unpack $ d2 `join` f2) :: IO (Either SomeException ())
          copyFile (T.unpack $ d1 `join` f1) (T.unpack $ d2 `join` f2)

        linker :: Text -> Text -> Text -> Text -> IO ()
        linker d1 f1 d2 f2 = do
          announce d1 f1 d2 f2
          _ <- try $ removeFile (T.unpack $ d2 `join` f2) :: IO (Either SomeException ())          
          createSymbolicLink (T.unpack $ d1 `join` f1) (T.unpack $ d2 `join` f2)

        announce :: Text -> Text -> Text -> Text -> IO ()
        announce d1 f1 d2 f2 = do
          let sarg = sformat (stext % "\n       :: " % stext  % "\n       :: " % stext)
                (T.pack . show $ action ls)
                (d1 `join` f1)
                (d2 `join` f2)
          TIO.putStrLn sarg

        join :: Text -> Text -> Text
        join a b =
          T.pack $ T.unpack a </> T.unpack b



        deployAction :: Text -> Text -> Text -> Text -> IO ()
        deployAction =
          case action ls of
            Symlink -> linker
            SymlinkToDotted  -> linker
            Copy -> copier
            CopyToDotted -> copier

        ensureDir :: Text -> IO ()
        ensureDir dirname = do
          TIO.putStrLn $ "Ensuring directory: " <> dirname
          let cdn = T.unpack dirname
          status <- classifyFilePath cdn
          case status of
            DNE -> do
              createDirectory cdn (CMode 0o755)
              TIO.putStrLn "(created)"
            _ -> pure ()

        -- cmode :: FileMode
        -- cmode = 
