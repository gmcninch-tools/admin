{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}


module Target
  (  Target (..)
   , TargetUsers (..)
   , TargetSystems (..)
   , Targetable (..)
   , targetsMatch
   )
  where

import Prelude
import Data.Text as T
import Data.List as DL

import UserSys

import qualified Dhall as Dh

data TargetUsers = AllUsers
                 | ExcludeUsers [ Text ]
                 | IncludeUsers [ Text ]
  deriving (Eq, Show, Dh.Generic)

data TargetSystems = AllSystems
                   | ExcludeSystems [ Text ]
                   | IncludeSystems [ Text ]
  deriving (Eq, Show, Dh.Generic)


data Target = Target { users :: TargetUsers
                     , systems :: TargetSystems}
  deriving (Eq, Show, Dh.Generic)

instance Dh.FromDhall TargetUsers
instance Dh.FromDhall TargetSystems
instance Dh.FromDhall Target

class Targetable a where
  getTargets :: a -> [ Target ]


targetsMatchUserSystem :: Targetable a => Text -> Text -> a -> Bool
targetsMatchUserSystem user system tt =
  or $ mat <$> getTargets tt
  where mat targ = matUser targ && matSystem targ

        matUser targ =
          case users targ of
            AllUsers -> True
            IncludeUsers ul -> user `DL.elem` ul
            ExcludeUsers ul -> user `DL.notElem` ul

        matSystem targ =
          case systems targ of
            AllSystems -> True
            IncludeSystems sl -> system `DL.elem` sl
            ExcludeSystems sl -> system `DL.notElem` sl


targetsMatch :: Targetable a => IO (a -> Bool)
targetsMatch =
    do
      us <- currentUserSys
      return $ targetsMatchUserSystem (curUser us) (curSystem us)
