{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DisambiguateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE NamedFieldPuns    #-}


module Cron
    ( cronEnv
    , setCron
    ) where


import Target
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Class (lift)
import Control.Monad (void)

import Data.Text as T
import Text.RawString.QQ
import Data.Text.IO as TIO

import Data.Function ((&))
import Data.Traversable (sequence, traverse)

import qualified Dhall as Dh
import Data.Maybe (Maybe(..), isJust, fromMaybe)

import Formatting

import Data.List as DL (map, filter, intercalate)

import Numeric.Natural

import System.Environment
import qualified System.Process as Proc

data Timing = Daily   { hour :: Maybe Natural }
            | Weekly  { dow :: [Natural]
                      , hour :: Maybe Natural
                      , minute :: Maybe Natural
                      }
            | PerDay  { hours :: [Natural] }
            | Hourly  { minute:: Maybe Natural }
            | PerHour { minutes:: [Natural] }
  deriving (Show, Dh.Generic)

data CronSpec = CronSpec
                { command :: Text
                , targets :: [Target]
                , timing :: Timing
                , output :: Text
                }
  deriving (Show, Dh.Generic)

instance Dh.FromDhall Timing
instance Dh.FromDhall CronSpec

instance Targetable CronSpec where
  getTargets cs = targets cs



cronEnv :: FilePath -> IO [CronSpec]
cronEnv = Dh.inputFile Dh.auto

setCron :: ReaderT [CronSpec] IO ()
setCron = do
  env <- ask
  tab <- lift $ runReaderT cronTab env
  let utab = T.unpack tab
  lift $ void $ Proc.readProcess "/usr/bin/crontab" [] $ utab ++ "\n"
  lift $ TIO.putStrLn tab


crontabHeader :: IO Text
crontabHeader = do
  username <- getEnv "USER"
  home <- getEnv "HOME"
  pure $ T.intercalate "\n"
    [ "#-------------<<-(oo)=(oo)->>-----------------"
    , "# min    hour    day     month   dow   command"
    , "# (0-59) (0-23)  (1-31)  (1-12)  (0-6) -------"
    , "#-------------<<-(oo)=(oo)->>-----------------"
    , "USER=" <> T.pack username
    , "HOME=" <> T.pack home
    , "SHELL=/bin/bash"
    , "MAILTO=george"
    , "#"
    ]

cronStringFromSpec :: CronSpec -> IO (Maybe Text)
cronStringFromSpec cs = do
  let cmd :: Text
      cmd = T.intercalate " " [". ~/.profile;"
                               , command cs
                               , "&> " <> output cs ]

      time :: Timing -> Text
      time (Daily (Just hour)) =
        sformat ("00 " % (left 2 '0' %. int) % " * * * " % stext )
        (toInteger hour)
        cmd
      time (Daily _) =
        sformat ("00 00 * * * %s" % stext)
        cmd
      time Weekly {dow, hour, minute}=
        let ahour = fromMaybe 0 hour
            aminute = fromMaybe 0 minute
        in
        sformat ( (left 2 '0' %. int) % " " % (left 2 '0' %. int) % " * * " % stext % " " % stext)
        (toInteger aminute)
        (toInteger ahour)
        (T.intercalate "," $ T.pack . show <$> dow)
        cmd
      time (PerDay hours) =
        T.intercalate "\n" $ DL.map (time . Daily . Just) hours
      time (Hourly (Just minute)) = sformat ((left 2 '0' %. int) % "  * * * * " % stext) (toInteger minute) cmd
      time (Hourly _) = sformat ("00 * * * * " % stext ) cmd
      time (PerHour minutes) =
        T.intercalate "\n" $ DL.map (time . Hourly . Just) minutes
    in
    pure $ Just $ time (timing cs)

cronTab :: ReaderT [CronSpec] IO Text
cronTab = do
  env <- ask
  pred <- lift targetsMatch
  dat <- lift $ traverse cronStringFromSpec $ DL.filter pred env
  header <- lift crontabHeader
  let filtered =
        fromMaybe [] $
        sequence $
        DL.filter isJust dat
  return $ T.intercalate "\n" $ header : filtered
