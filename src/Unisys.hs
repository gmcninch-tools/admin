-- Time-stamp: <2024-05-05 Sun 14:13 EDT - george@valhalla>
--
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE NamedFieldPuns    #-}

module Unisys
    ( unisysEnv
    , unisys
    ) where

import Target
import UserSys as US

import Control.Exception
import Control.Monad (void, when)

import Control.Monad.Trans.Reader
import Control.Monad.Trans.Class (lift)

import Data.Function ((&))
import Data.Traversable (sequence, traverse)
import Data.Foldable
import Data.Void

import Data.Either

import Data.Time

import Prelude
import qualified Dhall as Dh
import Data.Maybe (Maybe(..), isJust, fromMaybe)

import System.Directory
import System.FilePath
import System.IO.Error

import Data.Text as T
import Data.Text.IO as TIO
import Formatting

import Data.Bool as DB

import Data.List as DL (map, filter)
import Numeric.Natural

import qualified System.Process as Proc

data Stat = Primary | Secondary
  deriving (Eq, Show, Dh.Generic)

data UnisysRecord = UR { user :: Text
                       , setId :: Text
                       , pathList :: [Text]
                       , ignores :: [Text]
                       , system :: Text
                       , diskId :: Text
                       , mount :: Text
                       , status :: Stat
                       , connections :: [Text]
                   }
                  deriving (Show, Eq, Dh.Generic)

data RecordPair = RP {origin :: UnisysRecord,
                      target :: UnisysRecord }
  deriving (Show, Eq)

instance Dh.FromDhall Stat
instance Dh.FromDhall UnisysRecord


unisysEnv :: FilePath -> IO [UnisysRecord]
unisysEnv = Dh.inputFile Dh.auto


unisys :: Bool -> ReaderT [UnisysRecord] IO ()
unisys sync = do
  env <- ask
  lift $ runReaderT buildPrfs env
  when sync  $ lift syncPrfs

buildPrfs :: ReaderT [UnisysRecord]  IO ()
buildPrfs = do
  env <- ask
  lift removePrfs
  lift $ runReaderT writePrfsForUserSys' env


unisonDir :: Text -> IO FilePath
unisonDir user = do
    dir  <- US.userHomeDirectory $ T.unpack user
    return $ dir </> ".unison"

unisonDir' :: IO FilePath
unisonDir' = do
  us <- currentUserSys
  unisonDir (curUser us)

userSysRecords :: Text -> Text -> Reader [UnisysRecord] [UnisysRecord]
userSysRecords user system = do
  DL.filter (userSysP user system) <$> ask

userSysRecords' :: ReaderT [UnisysRecord] IO [UnisysRecord]
userSysRecords' = do
  env <- ask
  us <- lift currentUserSys
  return $ runReader (userSysRecords (curUser us) (curSystem us)) env


writePrfsForUserSys' :: ReaderT [UnisysRecord] IO ()
writePrfsForUserSys' = do
  env <- ask
  us <- lift currentUserSys
  writePrfsForUserSys
    (curUser us)
    (curSystem us)


writePrfsForUserSys :: Text -> Text -> ReaderT [UnisysRecord] IO ()
writePrfsForUserSys thisUser thisSys = do
  env <- ask 
  let usr :: [UnisysRecord]
      usr = DL.filter (statusP Primary) (runReader (userSysRecords thisUser thisSys) env)

      primaryPairs :: [RecordPair]
      primaryPairs = foldMap (\r -> runReader (getPrimaryMatches r) env) usr

      secondaryPairs :: [RecordPair]
      secondaryPairs = foldMap (\r -> runReader (getSecondaryMatches r) env) usr

  lift $ foldMap writePrf $ primaryPairs <>  secondaryPairs

getPrimaryMatches :: UnisysRecord -> Reader [UnisysRecord] [RecordPair]
getPrimaryMatches r = do
  env <- ask
  let pred :: UnisysRecord -> Bool
      pred = andP [ statusP Primary  -- "primary" filesets
                  , idP $ setId r    -- matching the given setId
                  , userP $ user r
                  , orP $ sysP <$> connections r
                  , notP $ sysP $ system r
                  ]
  return $ (\m -> RP { origin = r, target = m}) <$> DL.filter pred env


getSecondaryMatches :: UnisysRecord -> Reader [UnisysRecord] [RecordPair]
getSecondaryMatches r = do
  env <- ask
  let pred :: UnisysRecord -> Bool
      pred = andP [ statusP Secondary  -- "secondary" filesets
                  , idP $ setId r      -- matching the given setId
                  , userP $ user r
                  , sysP $ system r
                  ]

  return $ (\m -> RP { origin = r, target = m}) <$> DL.filter pred env


writePrf :: RecordPair -> IO ()
writePrf rp =
  do
    let prfOrigin :: UnisysRecord
        prfOrigin = origin rp

        prfTarget :: UnisysRecord
        prfTarget = target rp

    content <- prfText rp
    dirName <-  unisonDir $ user prfOrigin

    let fileName :: FilePath
        fileName = T.unpack $ sformat (stext % "--" % stext % "--" % stext % ".prf")
                   (system prfTarget)
                   (diskId prfTarget)
                   (setId prfTarget)

    TIO.writeFile (dirName </> fileName) content



andP :: [ UnisysRecord -> Bool ] -> (UnisysRecord -> Bool)
andP plist r = and $ (r &) <$> plist

orP :: [ UnisysRecord -> Bool ] -> (UnisysRecord -> Bool)
orP plist r = or $ (r &) <$> plist

notP :: ( UnisysRecord -> Bool ) -> ( UnisysRecord -> Bool )
notP f  = not . f

matchP :: Eq a => a -> (UnisysRecord -> a) -> UnisysRecord -> Bool
matchP val f r =val == f r

statusP :: Stat -> UnisysRecord -> Bool
statusP stat  = matchP stat status

idP :: Text -> UnisysRecord -> Bool
idP id = matchP id setId

userP :: Text -> UnisysRecord -> Bool
userP thisUser = matchP thisUser user

sysP :: Text -> UnisysRecord -> Bool
sysP thisSystem = matchP thisSystem system

userSysP :: Text -> Text -> UnisysRecord -> Bool
userSysP theuser thesys =
  andP [userP theuser, sysP thesys]




remove :: FilePath -> FilePath -> IO ()
remove dir fileName = do
  removeFile (dir </> fileName)

prfP :: FilePath -> Bool
prfP fp = ".prf" == takeExtension fp


removePrfs :: IO ()
removePrfs = do
  ud <- unisonDir'
  files <- listDirectory ud
  foldMap (remove ud) $ DL.filter prfP files


boxed :: Text -> Text
boxed s =
  T.intercalate "\n" [first,ss,last]
  where
    first = " ╔" <> (T.replicate (2 + T.length s)  "═") <> "╗"
    ss = " ║ " <> s <> " ║"
    last = " ╚" <> (T.replicate (2 + T.length s)  "═") <> "╝"
    

syncPrfs :: IO ()
syncPrfs = do
  us <- currentUserSys
  let syncCmd :: String -> IO ()
      syncCmd fp = do
        _ <- TIO.putStrLn ""
        _ <- TIO.putStrLn $ T.replicate 80 "─"
        _ <- TIO.putStrLn $ boxed $ "Syncing: " <> prf <> " for " <> (curUser us)
        (_,out,err) <- Proc.readProcessWithExitCode "unison" ["-batch",T.unpack prf] []
        Prelude.putStr err
        return ()
          where prf = T.pack $ takeBaseName fp

  ud <- unisonDir'
  files <- listDirectory ud
  foldMap syncCmd $ DL.filter prfP files



prfText :: RecordPair -> IO Text
prfText rp =  do
  time <- getZonedTime
  let prfOrigin = origin rp
      prfTarget = target rp

      remote_mount :: Text
      remote_mount  =
        if system prfOrigin == system prfTarget then
          mount prfTarget
          else
          sformat ("ssh://" % stext % "@" % stext % "//" % stext)
          (user prfTarget)
          (system prfTarget)
          (mount prfTarget)
      lines :: [Text]
      lines =
        [ sformat ("# Time-stamp: <" % stext % ">") $ pack (show time)
        , ""
        , sformat ("# setId: " % stext) $  setId prfOrigin
        , sformat ("root = " % stext) $ mount prfOrigin
        , sformat ("root = " % stext) remote_mount
        , ""
        ]
        <>
        DL.map (sformat ("path = " % stext)) (pathList prfOrigin)
        <>
        [""]
        <>
        DL.map (sformat ("ignore = " % stext)) (ignores prfOrigin)
        <>
        [ ""
        , "log = false"
        ]

  return $ intercalate "\n" lines

