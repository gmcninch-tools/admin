-- Time-stamp: <2022-07-11 Mon 12:31 EDT - george@valhalla>

module UserSys
  ( currentUserSys
  , UserSys(..)
  , userHomeDirectory
  )
where

import System.Posix.User (getLoginName, getEffectiveUserName, getUserEntryForName, UserEntry(..))
import System.Posix.Unistd (getSystemID, SystemID(..))
import Data.Text as T

data UserSys = US { curUser    :: Text
                  , curSystem  :: Text }

currentUserSys :: IO UserSys
currentUserSys = do
  --currentUser <- T.pack <$> getLoginName
  currentUser <- T.pack <$> getEffectiveUserName  
  --putStrLn $ T.unpack currentUser
  currentSystemID <- getSystemID
  pure (US { curUser = currentUser
            , curSystem = (T.pack . nodeName) currentSystemID } :: UserSys)


userHomeDirectory :: String -> IO FilePath
userHomeDirectory user = do
  entry <- getUserEntryForName user
  pure $ homeDirectory entry
  
